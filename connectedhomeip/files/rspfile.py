#Copyright (c) 2022, Qualcomm Innovation Center, Inc. All rights reserved.

#Permission to use, copy, modify, and/or distribute this software for any
#purpose with or without fee is hereby granted, provided that the above
#copyright notice and this permission notice appear in all copies.

#THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import sys
import os
import re

if __name__ == "__main__":
    fileName = sys.argv[1]
    dirName = sys.argv[2].replace('/', '_')
    outString = ''
    pat = re.compile(f"(\s+rspfile = __third_party_connectedhomeip_src_\w+){dirName}(_build_toolchain_custom_custom__rule.rsp)")
    for eachLine in open(fileName):
        m = pat.match(eachLine)
        if m:
            outString += (m.group(1) + m.group(2)+ os.linesep)
        else:
            if "--module pip -- install" in eachLine:
                eachLine = eachLine.rstrip() + " --target pip_pkg" + os.linesep
            outString += eachLine
    outFile = open(fileName, 'w')
    outFile.write(outString)
